# Singleton Pattern #

## What is it ? ##
The pattern which restrict the amount of objects to be only one object to be in the program.

## Motivation ##
It's beneficial for the situation which it has to use single object and reduce overlapping in the program processing.

![Singleton_UML.png](https://bitbucket.org/repo/ryqEoR/images/2997886178-Singleton_UML.png)

UML Diagram of Singleton Pattern

![Singleton_Sequence.png](https://bitbucket.org/repo/ryqEoR/images/3132770263-Singleton_Sequence.png)

Sequence Diagram of Singleton Pattern


```
#!java

/**
 * Singleton Pattern template (without factory method).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.26
 */
public class Singleton {
	private static Singleton instance = null;
	
	private Singleton() {
		// Default constructor
	}

	public static Singleton getInstance() {
		if(instance == null) {
			instance = new Singleton();
		}

		return instance;
	}
	
	public void doSomething() {
		// do something...
	}
}

```


## How to Use ##
There are many ways to use a Singleton Pattern 

1.) Hide constructor from other classes to invoke by set it to be private and use getInstance() method instead. Therefore, other classes can’t access the constructor. For example:



```
#!java

/**
 * Simple Database class which use Singleton Pattern (with factory method).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.26
 */
public class Database {
	private static Database database = null;
	
	private Database() {
		// Default constructor
	}

	/**
	 * This is a public factory method.
	 * @return instance of object
	 */
	public static Database getInstance() {
		if(database == null) {
			database = new Database();
		}

		return database;
	}
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database database1 = Database.getInstance();
		database1.doSomething();
		Database database2 = Database.getInstance();
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}

```

You can also do like this


```
#!java

/**
 * Simple Database class which use Singleton Pattern (without factory method).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.27
 */
public class Database {
	public static final Database database = new Database();
	
	private Database() {
		// Default constructor
	}
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database database1 = Database.database;
		database1.doSomething();
		Database database2 = Database.database;
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}

```

TODO: Run this code and see the results.

Here are the results:


```
#!java
Created New Database
Created New Database
Same Database: true

```

As you can see, the program is the wrong example of using Singleton (Database should have just only one). However, I would like to show you that whether we create more than one Database, but they are the same Database.

### Pros: ###
This can assure that there will be just only one instance (actually this is the property of Singleton)

### Cons: ###
Users can invoke private constructor by using this method below:


```
#!java
AccessibleObject.setAccessible(true);

```


To avoid this situation, add the condition checking that instance is not null, then throw exception.




2.) Using Serialization, in case that we would like our programs to read/write files.
We have to create readResolve() method for ensuring that nobody can create another instance by serializing and deserializing the Singleton.


```
#!java

import java.io.Serializable;

/**
 * Simple Database class which use Singleton Pattern (with Serialization).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.27
 */
public class Database implements Serializable {
	private static Database database = null;
	
	private Database() {
		// Default constructor
	}

	/**
	 * This is a public factory method.
	 * @return instance of object
	 */
	public static Database getInstance() {
		if(database == null) {
			database = new Database();
		}

		return database;
	}
	
	/**
	 * @return instance which was serialized, new-created instance will be
	 * removed by Garbage Collector (GC)
	 */
	private Object readResolve() {
		return database;
	}
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database database1 = Database.getInstance();
		database1.doSomething();
		Database database2 = Database.getInstance();
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}

```

### Pros: ###
Nobody can create another instance by serializing and deserializing the Singleton.

### Cons: ###
The code is very complicated, kind of hard to read.

3.) Using Enumeration, this is the best solution for Singleton Pattern because we can assure that there is only one instance and this prevents anyone create new instance by using serialization. Moreover, this code is quite short so it's easy to read.


```
#!java

/**
 * Simple Database class which use Singleton Pattern (enumeration).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.27
 */
public enum Database {
	DATABASE;

        private Database() {

        }
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database database1 = Database.DATABASE;
		database1.doSomething();
		Database database2 = Database.DATABASE;
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}

```

## Exercise ##

Create a class called **NumberGenerator** which random number from 1-100 with any method.