/**
 * Simple Database class which use Singleton Pattern (without factory method).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.27
 */
public class Database2 {
	public static final Database2 database = new Database2();
	
	private Database2() {
		// Default constructor
	}
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database2 database1 = Database2.database;
		database1.doSomething();
		Database2 database2 = Database2.database;
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}
