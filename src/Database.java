/**
 * Simple Database class which use Singleton Pattern (with factory method).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.26
 */
public class Database {
	private static Database database = null;
	
	private Database() {
		// Default constructor
	}

	/**
	 * This is a public factory method.
	 * @return instance of object
	 */
	public static Database getInstance() {
		if(database == null) {
			database = new Database();
		}

		return database;
	}
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database database1 = Database.getInstance();
		database1.doSomething();
		Database database2 = Database.getInstance();
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}
