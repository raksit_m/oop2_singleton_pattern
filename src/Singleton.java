/**
 * Singleton Pattern template (without factory method).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.26
 */
public class Singleton {
	private static Singleton instance = null;
	
	private Singleton() {
		// Default constructor
	}

	public static Singleton getInstance() {
		if(instance == null) {
			instance = new Singleton();
		}

		return instance;
	}
	
	public void doSomething() {
		// do something...
	}
}
