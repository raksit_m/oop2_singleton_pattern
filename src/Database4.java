/**
 * Simple Database class which use Singleton Pattern (enumeration).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.27
 */
public enum Database4 {
	DATABASE;
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database4 database1 = Database4.DATABASE;
		database1.doSomething();
		Database4 database2 = Database4.DATABASE;
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}
