import java.io.Serializable;

/**
 * Simple Database class which use Singleton Pattern (with Serialization).
 * @author Raksit Mantanacharu
 * @author Sanrasern Chaihetphon
 * @version 2015.04.27
 */
public class Database3 implements Serializable {
	private static Database3 database = null;
	
	private Database3() {
		// Default constructor
	}

	/**
	 * This is a public factory method.
	 * @return instance of object
	 */
	public static Database3 getInstance() {
		if(database == null) {
			database = new Database3();
		}

		return database;
	}
	
	/**
	 * @return instance which was serialized, new-created instance will be
	 * removed by Garbage Collector (GC)
	 */
	private Object readResolve() {
		return database;
	}
	
	public void doSomething() {
		System.out.println("Created New Database");
	}
	
	public static void main(String[] args) {
		Database3 database1 = Database3.getInstance();
		database1.doSomething();
		Database3 database2 = Database3.getInstance();
		database2.doSomething();
		System.out.println("Same Database: " + (database1 == database2));
	}
}
